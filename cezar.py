letter_to_code_map = { 'a':0, 'b':1, 'c':2, 'd':3, 'e':4, 'f':5, 'g':6, 'h':7, 'i':8, 'j':9, 'k':10, 'l':11, 'm':12, 'n':13, 'o':14, 'p':15, 'q':16, 'r':17, 's':18, 't':19, 'u':20, 'v':21, 'w':22, 'x':23, 'y':24, 'z':25}

code_to_letter_map = dict([(item[1], item[0]) for item in letter_to_code_map.items()])

def c(x):
   c = x + 3
   if c > 25:
      return c - 26
   else:
      return c

def x(c):
   x = c - 3
   if x < 0:
      return x + 26
   else:
      return x

def letter_to_caesar(letter):
   if letter in letter_to_code_map:
      x = letter_to_code_map.get(letter)
      caesar_letter_code = c(x)
      return code_to_letter_map.get(caesar_letter_code)
   else:
      return letter

def caesar_to_letter(caesar):
   if caesar in letter_to_code_map:
      c = letter_to_code_map.get(caesar)
      letter_code = x(c)
      return code_to_letter_map.get(letter_code)
   else:
      return caesar
      
def encode(text):
   if (len(text) == 0):
      return ""
   return ''.join([letter_to_caesar(letter) for letter in text])

def decode(text):
   if (len(text) == 0):
      return ""
   return ''.join([caesar_to_letter(caesar) for caesar in text])