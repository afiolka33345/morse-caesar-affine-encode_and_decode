letter_to_code_map = { 'a':0, 'b':1, 'c':2, 'd':3, 'e':4, 'f':5, 'g':6, 'h':7, 'i':8, 'j':9, 'k':10, 'l':11, 'm':12, 'n':13, 'o':14, 'p':15, 'q':16, 'r':17, 's':18, 't':19, 'u':20, 'v':21, 'w':22, 'x':23, 'y':24, 'z':25}

code_to_letter_map = dict([(item[1], item[0]) for item in letter_to_code_map.items()])

def f(x, a, b):
   f = (a * x + b) % 26
   return f

valid_a = [1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23, 25]

def is_valid(a, b):
   return a in valid_a and b > 0

def letter_to_affine(letter, a, b):
   if letter in letter_to_code_map:
      x = letter_to_code_map.get(letter)
      affine_letter_code = f(x, a, b)
      return code_to_letter_map.get(affine_letter_code)
   else:
      return letter
      
def encode(text, a, b):
   if (len(text) == 0):
      return ""
   return ''.join([letter_to_affine(letter, a, b) for letter in text])