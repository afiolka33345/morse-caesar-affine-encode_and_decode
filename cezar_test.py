import unittest
import cezar

class CaesarTest(unittest.TestCase):

  def test_encode(self):
    self.assertEqual("", cezar.encode(""))
    self.assertEqual("def", cezar.encode("abc"))
    self.assertEqual("d e", cezar.encode("a b"))
    self.assertEqual("abc", cezar.encode("xyz"))

  def test_decode(self):
    self.assertEqual("", cezar.decode(""))
    self.assertEqual("abc", cezar.decode("def"))
    self.assertEqual("a b", cezar.decode("d e"))
    self.assertEqual("xyz", cezar.decode("abc"))

if __name__ == '__main__':
    unittest.main()