# morse-caesar-affine-encode_and_decode

A program that encodes/decodes texts in Morse code and Caesar's cipher and encodes texts in affine cipher.

**All the files in the project HAVE TO BE in the same folder!!!**

To run the program use command ```python main.py```

To run unit tests use command ```python -m unittest discover -p "*_test.py"```