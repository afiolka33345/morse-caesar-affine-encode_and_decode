to_morse_map = { 'a':'.-', 'b':'-...',
   'c':'-.-.', 'd':'-..', 'e':'.',
   'f':'..-.', 'g':'--.', 'h':'....',
   'i':'..', 'j':'.---', 'k':'-.-',
   'l':'.-..', 'm':'--', 'n':'-.',
   'o':'---', 'p':'.--.', 'q':'--.-',
   'r':'.-.', 's':'...', 't':'-',
   'u':'..-', 'v':'...-', 'w':'.--',
   'x':'-..-', 'y':'-.--', 'z':'--..',
   '1':'.----', '2':'..---', '3':'...--',
   '4':'....-', '5':'.....', '6':'-....',
   '7':'--...', '8':'---..', '9':'----.',
   '0':'-----', ',':'--..--', '.':'.-.-.-',
   '?':'..--..', '/':'-..-.', '-':'-....-',
   '(':'-.--.', ')':'-.--.-', 'ą':'.-.-',
   'ć':'-.-..', 'ę':'..-..', 'ł':'.-..-',
   'ń':'--.--', 'ó':'---.', 'ś':'...-...',
   'ż':'--..-.', 'ź':'--..-',
}

from_morse_map = dict([(item[1], item[0]) for item in to_morse_map.items()])

def letter_to_morse(letter):
  return to_morse_map[letter.lower()];

def word_to_morse(word):
  return " ".join([letter_to_morse(letter) for letter in word])

def morse_sign_to_letter(morse_sign):
  return from_morse_map[morse_sign]

def morse_word_to_word(morse_word):
  return "".join([morse_sign_to_letter(morse_sign) for morse_sign in morse_word.split(' ')])

def encode(text):
  if (len(text) == 0):
    return ""
  return "     ".join([word_to_morse(word) for word in text.split(' ')])

def decode(text):
  if (len(text) == 0):
    return ""
  return " ".join([morse_word_to_word(morse_word) for morse_word in text.split('     ')])