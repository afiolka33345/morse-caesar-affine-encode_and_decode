import morse
import cezar
import affine

def get_file_text():
  while True:
    try:
      file_name = input("Podaj nazwę pliku do odczytu:\n")
      file = open(file_name, 'r')
      text = file.read()
      return text
    except:
      continue

def save_to_file(text):
  while True:
    do_save = input("Czy chcesz zapisać? ([t]ak/[n]ie):")
    if do_save == 't':
      try:
        file_name = input("Podaj nazwę pliku do zapisu:\n")
        file = open(file_name, 'w')
        file.write(text)
        return
      except:
        continue
    elif do_save == 'n':
      return

def main():
  while True: 
    szyfr = input("Wybierz kod: 1 - Kod Morse'a, 2 - Szyfr Cezara, 3 - Szyfr Afiniczny, 4 - Wyjście z programu\n") 
    if szyfr == '1':
      operacja = input("[o]dszyfruj lub [s]zyfruj\n")
      if operacja == 'o':
        text = morse.decode(get_file_text())
        print(text)
        save_to_file(text)
      elif operacja == 's':
        text = morse.encode(get_file_text())
        print(text)
        save_to_file(text)
    elif szyfr == '2': 
        operacja = input("[o]dszyfruj lub [s]zyfruj\n")
        if operacja == 'o':
          text = cezar.decode(get_file_text())
          print(text)
          save_to_file(text)
        elif operacja == 's':
          text = cezar.encode(get_file_text())
          print(text)
          save_to_file(text)
    elif szyfr == '3':
      while True:
        a = int(input("Podaj pierwszą liczbę klucza (1, 3, 5, 7, 9, 11, 15, 17, 19, 21, 23 lub 25): "))
        b = int(input("Podaj drugą liczbę klucza: "))
        if affine.is_valid(a, b):
          text = affine.encode(get_file_text(), a, b)
          print(text)
          save_to_file(text)
          break
        else:
          print("Niepoprawne liczby klucza. Wpisz np. 7 i 5.")
          continue
    elif szyfr == '4': 
      break
    else: 
      continue

if __name__ == '__main__':
  main()