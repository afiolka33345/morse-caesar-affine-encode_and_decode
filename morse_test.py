import unittest
import morse

class MorseTest(unittest.TestCase):

  def test_encode(self):
    self.assertEqual("", morse.encode(""))
    self.assertEqual(".- -... -.-.", morse.encode("abc"))
    self.assertEqual(".-     -...", morse.encode("a b"))

  def test_decode(self):
    self.assertEqual("", morse.decode(""))
    self.assertEqual("abc", morse.decode(".- -... -.-."))
    self.assertEqual("a b", morse.decode(".-     -..."))

if __name__ == '__main__':
    unittest.main()