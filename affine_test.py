import unittest
import affine

class CaesarTest(unittest.TestCase):

  """
  Przyjmując, że K = (7, 5) należy zaszyfrować słowo KOT.
  Tekst zaszyfrowany odpowiada ciągowi: XZI.
  """
  def test_encode(self):
    self.assertEqual("xzi", affine.encode("kot", 7, 5))

if __name__ == '__main__':
    unittest.main()